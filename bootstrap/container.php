<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 5/30/17
 * Time: 10:31 PM
 */

use App\Helpers\GShortUrl;
use App\Helpers\Randomizer;
use Noodlehaus\Config;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Illuminate\Database\Capsule\Manager as Capsule;

use Slim\Flash\Messages;
use Slim\Csrf\Guard;

use App\Controllers\HomeController;
use App\Controllers\AuthController;
use App\Controllers\AdminController;
use App\Controllers\RMSController;

use App\Auth\Auth;
use App\Auth\oAuth;

use App\Validation\Validator;


$container = $app->getContainer();

$container['config'] = function ($container) {
    return new Config(
        ROOTPATH . DS . 'app' . DS . 'Config' . DS . APPMODE
    );
};

$container['view'] = function ($container) {            // Add the Twig View dependency
    $view = new Twig(
        $container
            ->config
            ->get('view.template'), [
        'cache' =>  $container->config->get('view.cache'),
        'debug' =>  $container->config->get('view.debug')
    ]);
    $view
        ->addExtension (
        new TwigExtension (
            $container->router,
            $container->request->getUri()
        )
    );
    $view
        ->getEnvironment()
        ->addGlobal(
            'URI', [
                'FullPath'      =>  $container->request->getUri(),
                'Path'          =>  $container->request->getUri()->getPath(),
                'BasePath'      =>  $container->request->getUri()->getBasePath()
            ]
        );

//    Globalize the Auth module to access it within template partials
    $view
        ->getEnvironment()
        ->addGlobal ('auth', [
            'check'         	=>  $container->auth->check(),
            'user'          	=>  $container->auth->user(),
            'Users'             =>  $container->auth->allUsers()
            ]);

    // Add the flash message
    $view
        ->getEnvironment()
        ->addGlobal(
            'flash',
            $container->flash
        );

    return $view;
};

// DB Connection (using Illuminate DB Manager)
$capsule = new Capsule;
$capsule->addConnection($container->config->get('db'));
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

// Add the Controllers:
$container['HomeController'] = function ($container) {
    return new HomeController();
};

$container['AuthController'] = function ($container) {
    return new AuthController($container);
};

$container['AdminController'] = function ($container) {
    return new AdminController($container);
};

$container['RMSController'] = function ($container) {
    return new RMSController($container);
};

// Add the validation dependency
$container['validator'] = function ($container) {
    return new Validator();
};

$container['flash'] = function ($container) {
    return new Messages();  // Add global message flash dependency
};

$container['csrf'] = function ($container) {
    return new Guard();
};

$container['auth'] = function ($container) {
    return new Auth($container);
};

$container['oAuth'] = function ($container) {
    return new oAuth($container);
};

$container['ShortUrl'] = function ($container) {
    $key = $container->config->get('google.shorturlAPI');
    return new GShortUrl($key);
};

$container['Randomizer'] = function ($container) {
    return new Randomizer($container);
};

$container['cache'] = function ($container) {
//    return new \App\Middleware\HttpCache\CacheProvider();             // Slim HttpCache
    return new \Slim\HttpCache\CacheProvider();
};

// Override notFoundHandler (404):
$container['notFoundHandler'] = function ($container) {
    return new App\Handlers\NotFoundHandler($container['view']);
};

// Override notAllowedHandler (405):
$container['notAllowedHandler'] = function ($container) {
    return new App\Handlers\notAllowedHandler($container['view']);
};

// Override phpErrorHandler (500):
$container['phpErrorHandler'] = function ($container) {
    return new App\Handlers\phpErrorHandler($container['view']);
};

// Override CsrfErrorHandler (CSRF):
$container['CsrfErrorHandler'] = function ($container) {
    return new App\Handlers\CsrfErrorHandler($container['view']);
};