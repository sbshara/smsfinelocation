<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 6/6/17
 * Time: 4:16 PM
 */

namespace App\Validation\Rules;

use App\Models\Locatordata;
use Respect\Validation\Rules\AbstractRule;

class CaseAvailable extends AbstractRule {

	public function validate ($input) {
		return Locatordata::where('case_id', $input)->count() === 0;
	}

}
