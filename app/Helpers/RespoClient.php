<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 6/30/17
 * Time: 12:19 AM
 */

namespace App\Helpers;

class RespoClient {
    protected $container;

    function __construct($container) {
        $this->container = $container;
    }


    /**
     *
     * @target = string (link)
     * @method = string
     * @data = array()
     *
     */



    public function index ($target = '', $method = 'post', $data = []) {
        // convert $data array into a json string:
        $json_data = json_encode($data);

        // use curl to trigger the method to target (include options):
        $curl = curl_init($target);

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array (
                'Content-Type: application/json',
                'Content-Length:' . strlen($json_data)
            )
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);

        // send the request:
        $result = curl_exec($curl);

        // release curl resources
        curl_close($curl);

        return $result;

    }
}