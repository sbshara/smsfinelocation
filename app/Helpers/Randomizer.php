<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 6/30/17
 * Time: 12:19 AM
 */

namespace App\Helpers;

class Randomizer {
    protected $container;
    protected $len;
    protected $charset;


    function __construct($container) {
        $this->container = $container;
        $this->len = $this->container->config->get('app.randomizer.length');
        $this->charset = $this->container->config->get('app.randomizer.charset');
    }

    function index ($len, $charset) {
        $str = "";
        $characters = $charset;
        $max = count($characters) - 1;
        for ($i = 0; $i < $len; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }


}