<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 6/6/17
 * Time: 11:00 PM
 */

namespace App\Middleware;


class oAuthMiddleware extends Middleware {


    /**
     * @param $request PSR-7
     * @param $response
     * @param $next
     * @return HTML: 2 input:hidden (CSRF NAME & CSRF VALUE)
     */
    public function __invoke ($request, $response, $next) {
        if (!$this->container->oAuth->check($request)) {
            return $response->withStatus(401)->withJson([
                'response'  =>  'Error',
                'error'     =>  'You are not authorized to perform this action',
                'errorno'   =>  401
            ]);
        }
        $response = $next($request, $response);
        return $response;
    }


}