<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 5/30/17
 * Time: 8:50 PM
 */

use App\Controllers\HomeController;
use App\Controllers\AuthController;
use App\Controllers\AdminController;
use App\Controllers\RMSController;

use App\Middleware\AuthMiddleware;
use App\Middleware\oAuthMiddleware;
use App\Middleware\GuestMiddleware;
use \App\Middleware\CsrfViewMiddleware;


// CSRF Guarded Group:
//$app->group('/', function () {

$app->get('/', HomeController::class . ':redirectHome');

    $app->group('/guest', function () {
        // Public home page
        $this->get('/', HomeController::class . ':publicIndex')->setName('home');
        $this->get('/signup', AuthController::class . ':getSignUp')->setName('auth.SignUp');
        $this->post('/signup', AuthController::class . ':postSignUp');
        $this->get('/signin', AuthController::class . ':getSignIn')->setName('auth.SignIn');
        $this->post('/signin', AuthController::class . ':postSignIn');
        // Customer accessed links
        $this->get('/fine/location/{case}/{token}', RMSController::class . ':getCustomerMap')->setName('customer.map');
        $this->get('/Thank/You', RMSController::class . ':getResponse')->setName('post.customer.map');
        $this->post('/Thank/You', RMSController::class . ':postCustomerMap');
//    })->add($container->csrf);
    });


    // Authenticated (User Interface)
    $app->group('/auth', function () {
        // Logged in Home page
        $this->get('/', HomeController::class . ':index')->setName('private.home');
        $this->get('/signout/', AuthController::class . ':getSignOut')->setName('auth.SignOut');
        $this->get('/password/change/', AuthController::class . ':getPasswordChange')->setName('auth.password.change');
        $this->post('/password/change/', AuthController::class . ':postPasswordChange');
        // Admin Module
        $this->group('admin', function () {

            $this->group('/user', function () {
                // get all users
                $this->get('s/', AdminController::class . ':getAllUsers')->setName('user.all');

                // get user by id
                $this->get('/update/{user_id}', AdminController::class . ':getUserById')->setName('user.id');
                $this->post('/update/{user_id}', AdminController::class . ':postUserById');

                // create new user
                $this->get('/new', AdminController::class . ':getNewUser')->setName('user.new');
                $this->post('/new', AdminController::class . ':postNewUser');

                // Change user password
                $this->get('/password/reset/{user_id}', AdminController::class . ':getUserPasswordReset')->setName('user.password');
                $this->post('/password/reset/{user_id}', AdminController::class . ':postUserPasswordReset');

                // delete user
                $this->get('/delete/{user_id}', AdminController::class . ':deleteUser')->setName('user.delete');
            });

            $this->group('/portal', function () {
                // get all users
                $this->get('s/', AdminController::class . ':getAllPortals')->setName('portal.all');

                // get user by id
                $this->get('/update/{portal_id}', AdminController::class . ':getPortalById')->setName('portal.id');
                $this->post('/update/{portal_id}', AdminController::class . ':postPortalById');

                // create new user
                $this->get('/new', AdminController::class . ':getNewPortal')->setName('portal.new');
                $this->post('/new', AdminController::class . ':postNewPortal');

                // delete user
                $this->get('/delete/{portal_id}', AdminController::class . ':deletePortal')->setName('portal.delete');
            });

        });
    })->add([
        new AuthMiddleware($container),
        $container->csrf
    ]);

$app->group('/api', function () {

    // test Fritz
    $this->get('/fritz', RMSController::class . ':fritzTest')->setName('fritz');

    $this->group('/fetch', function () {
        $this->get('/all', RMSController::class . ':fetchAll')->setName('fetch.all');
    });

    $this->group('/generate', function () {
        $this->get('/all/{case}[/{language}[/[brand]]]', RMSController::class . ':generateAll')->setName('generate.all');
        $this->get('/token/{case}[/{language}[/brand]]', RMSController::class . ':generateToken')->setName('generate.token');
        $this->get('/url/{case}[/{language}[/brand]]', RMSController::class . ':generateUrl')->setName('generate.url');
        $this->get('/shorturl/{case}[/{language}[/brand]]', RMSController::class . ':generateShortUrl')->setName('generate.shortUrl');
    });
})->add(new oAuthMiddleware($container));

$app->group('/log', function () {
    $this->post('/in', AuthController::class . ':APIlogin')->setName('api.login');
    $this->get('/out', AuthController::class . ':APIlogout')->setName('api.logout');
});