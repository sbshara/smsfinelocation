<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 5/30/17
 * Time: 8:51 PM
 */

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Language;
use App\Models\Locatordata;
use App\Models\Brand;
use App\Models\Status;
use Interop\Container\ContainerInterface;
use Respect\Validation\Validator as v;


class RMSController extends Controller {

    public function fetchAll($request, $response, $args) {
        $brands = Brand::all();
        $languages = Language::all();
        $statuses = Status::all();

        return $response->withStatus(200)->withJson([
            'brands'    =>  $brands,
            'languages' =>  $languages,
            'statuses'  =>  $statuses
        ]);
    }

    public function generateToken($request, $response, $args) {}
    public function generateUrl($request, $response, $args) {}
    public function generateShortUrl($request, $response, $args) {}

    public function generateAll ($request, $response, $args) {
        // get the variables from the sent $request
        $case = $request->getAttribute('case');
        $lan = $request->getAttribute('language', 1); // 0 for english as default
        $brand = $request->getAttribute('brand', 1);
        // TODO: Validation
//        $validation = $this->container->validator->validate($request, [
//            'case'              =>  v::CaseAvailable()
//        ]);
//        if ($validation->failed()) {
//            return $response->withStatus(302)->withJson([
//                'response'      =>  'Error',
//                'error'         =>  'Case ID validation failed',
//                'errorno'       =>  302
//            ]);
//        }

        // Generate Token
        $len = $this->container->config->get('app.ramdomizer.length');
        $char = $this->container->config->get('app.ramdomizer.charset');
        $token = $this->container->Randomizer->index($len, $char);

        // Construct URL
        $base = $this->container->config->get('app.siteUrl');
        $url = $base . DS . 'guest' . DS . 'fine' . DS . 'location' . DS . $case . DS . $token;

        // get Short URL
        $shortUrl = $this->container->ShortUrl->shorten($url);

        // create the record
        $newRec = Locatordata::create([
            'case_id'       =>  $case,
            'language'      =>  $lan,
            'brand'         =>  $brand,
            'token'         =>  $token,
            'full_url'      =>  $url,
            'short_url'     =>  $shortUrl,
            'status'        =>  1,
            'longitude'     =>  null,
            'latitude'      =>  null,
            'csrcomment'    =>  null
        ]);
//        $jsonRec = json_encode($newRec);

        // respond with JSON
        return $response->withJson([
            'case' => $case,
            'language' => $lan,
            'brand' => $brand,
            'token' => $token,
            'url' => $url,
            'shorturl' => $shortUrl
        ]);

    }

    public function getCustomerMap ($request, $response, $args) {
        $arg = $args;
        return $this->view->render($response, 'rmsweb/index.twig', compact('arg'));
    }

    public function getResponse ($request, $response, $args) {
        $arg = $args;
        return $this->view->render($response, 'rmsweb/response.twig', compact('arg'));
    }

    public function postCustomerMap ($request, $response, $args) {
        // Validation
        // Submit the details locally
        // Submit the details to the curl (menaa address)
        $case = $request->getParam('case');
        $token = $request->getParam('token');
        $lng = $request->getParam('lng');
        $lat = $request->getParam('lat');
        $comment = $request->getParam('comment');

        $record = Locatordata::where('case_id', $case);

        $record->update([
            'longitude'     =>  $lng,
            'latitude'      =>  $lat,
            'csrcomment'    =>  $comment
        ]);

        /**
         * the below REST POST Request is taken from:
         * https://stackoverflow.com/questions/27440171/how-to-send-a-post-request-to-the-restserver-api-in-php-codeigniter
         */
        $data = array(
            'caseid'    =>  $case,
            'token'     =>  $token,
            'longvalue' =>  $lng,
            'latvalue'  =>  $lat,
            'comment'   =>  $comment
        );

        $dataString = json_encode($data);

//        echo $dataString;
//        die();

        $clientAddress = $this->config->get('client.url');
        $clientMethod = $this->config->get('client.method');
        $curl = curl_init($clientAddress);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $clientMethod);
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($dataString)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataString); // Insert the data\
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);
        $record->update([
            'status'  =>  $result
        ]);
        return $this->view->render($response, 'rmsweb/response.twig', compact('data'));
    }

    public function fritzTest ($request, $response, $args) {
        $data = array(
            'loginId'   =>  'mena.assistance@carpal.com',
            'password'  =>  '7FQ-ErhX*v2S3^kbtL5N^?V%'
        );

        $dataString = json_encode($data);

//        echo $dataString;
//        die();

        $clientAddress = 'http://rsa.carpal.com/user/validate';
        $clientMethod = 'POST';
        $curl = curl_init($clientAddress);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $clientMethod);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($dataString)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Make it so the data coming back is put into a string
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataString); // Insert the data\
        // Send the request
        $result = curl_exec($curl);
        // Free up the resources $curl is using
        curl_close($curl);

        return $response->withJson([
            'response'      =>  $result
        ]);
    }

}