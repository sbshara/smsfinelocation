<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 5/30/17
 * Time: 8:51 PM
 */

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Locatordata;


class HomeController extends Controller {

    public function index ($request, $response, $args) {
        return $this->view->render($response, 'home.twig');
    }

    public function publicIndex ($request, $response, $args) {
        return $this->view->render($response, 'public.twig');
    }

    public function redirectHome ($request, $response, $args) {
        return $response->withRedirect($this->container->router->pathFor('home'));
    }
}