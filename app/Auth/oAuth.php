<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 6/12/17
 * Time: 1:55 PM
 */

namespace App\Auth;

use App\Models\OAuthTokens;
use App\Models\User;

class oAuth {

    protected $container;

    function __construct ($container) {
        $this->container = $container;
    }

    public function check ($request) {
        return $this->oAuth($request->getParam('token'));
    }

    public function oAuth ($token) {
        return OAuthTokens::where('token', $token)->get();
    }


}