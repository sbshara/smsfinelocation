<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 7/5/17
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    protected $table = 'language';

    protected $fillable = [
        'language_name',
        'language_code'
    ];








}