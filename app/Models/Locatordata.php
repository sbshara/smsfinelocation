<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 7/5/17
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Locatordata extends Model {

    protected $table = 'locatordata';

    protected $fillable = [
        'case_id',
        'language',
        'brand',
        'domain',
        'token',
        'full_url',
        'short_url',
        'status',
        'longitude',
        'latitude',
        'csrcomment'
    ];








}