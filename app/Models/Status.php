<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 7/5/17
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $table = 'status';

    protected $fillable = [
        'status_name',
        'description'
    ];








}