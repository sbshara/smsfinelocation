<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 7/5/17
 * Time: 6:39 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OAuthTokens extends Model {

    protected $table = 'oauthtokens';
    protected $fillable = [
        'user_id',
        'token',
        'valid',
        'created_at',
        'updated_at'
    ];
}