<?php
/**
 * Created by PhpStorm.
 * User: shiblie
 * Date: 7/5/17
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Brand extends Model {

    protected $table = 'brand';

    protected $fillable = [
        'brand_name',
        'brand_name_ar',
        'brand_code',
        'description',
        'domain',
        'logo'
    ];








}